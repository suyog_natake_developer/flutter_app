const restify = require('restify');
const Config = require('./config');
const HomeRouter = require('./app/routes/homeRoute');


const server = restify.createServer({
	name: Config.name,
	version: Config.version,
	url: Config.hostname,
});

const corsMiddleware = require("restify-cors-middleware");

var cors = corsMiddleware({
	origins: ["*"],
	allowHeaders: ["Authorization"],
	exposeHeaders: ["Authorization"]
});

server.pre(cors.preflight);
server.use(cors.actual);

server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
// server.use(restify.plugins.bodyParser());
server.use(restify.plugins.jsonBodyParser());


HomeRouter(server);


const appPort = Config.port;
server.listen(appPort, (err) => {
	if (err) {
		console.log(err);
	} else {
		console.log(`server is running on port http://localhost:${appPort}`);

	}
});
