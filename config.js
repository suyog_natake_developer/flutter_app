module.exports = {
	name: 'apache',
	hostname: 'localhost',
	encryptionKey: '#@RAVIRAPAHTARAM@#',
	version: '0.0.1',
	env: process.env.NODE_ENV || 'production',
	port: 5000,
	errCodeError: -1,
	errCodeNoRecordFound: 1,
	errCodeSuccess: 0,
	TOKEN_SECRET: process.env.TOKEN_SECRET,

	ACCESS_TOKEN_SECRET: process.env.ACCESS_TOKEN_SECRET,
	S3_ACCESS_KEY: process.env.S3_ACCESS_KEY,
	S3_ACCESS_SECRET: process.env.S3_ACCESS_SECRET,


	Database_URI: process.env.Database_URI,
	NODE_ENV: process.env.NODE_ENV,
	JWT_SECRET: process.env.JWT_SECRET,
	ORIGIN: process.env.ORIGIN,

	FAST2SMS: process.env.FAST2SMS,
	ADMIN_PHONE: process.env.ADMIN_PHONE
}

