const path = require('path')
const multer = require('multer');
const multer_s3 = require('multer-s3');
const aws = require('aws-sdk');
const config = require('./config')
// CONFIGURATION OF S3
aws.config.update({
	secretAccessKey: process.env.S3_ACCESS_SECRET,
	accessKeyId: process.env.S3_ACCESS_KEY,
	region: "us-east-2",
});

// CREATE OBJECT FOR S3
var s3 = new aws.S3();


const fileFilter = (req, file, cb) => {
	if (file.mimetype === "image/png" || file.mimetype === "image/jpeg" || file.mimetype === "image/jpg") {
		cb(null, true);
	} else {
		cb(new Error("Error: Allow images only of extensions jpeg|jpg|png !"))
	}
}

// MULTER FUNCTION FOR UPLOAD

var upload = multer({
	fileFilter,
	// CREATE MULTER-S3 FUNCTION FOR STORAGE
	storage: multer_s3({
		s3: s3,
		bucket: 'marathapariwarstore',
		acl: 'public-read',
		contentType: multer_s3.AUTO_CONTENT_TYPE,

		metadata: function (req, file, cb) {
			// META DATA FOR PUTTING FIELD NAME
			console.log("Called when saving image  to AWS");
			cb(null, { fieldName: file.fieldName });
		},
		key: function (req, file, cb) {
			console.log("FILE ==> " + JSON.stringify(file));
			cb(null, Date.now().toString() + '-' + file.originalname);        //    use Date.now() for unique file keys
		},
	})
})


module.exports = upload;

