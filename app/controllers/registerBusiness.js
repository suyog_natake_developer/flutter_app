const RegisterBusiness = require('./../models/registerBusiness');
const upload = require('./../../testMulter')
module.exports = {
	registerBusiness: (req, res) => {
		RegisterBusiness.registerBusiness(req, res, result => {
			res.send(result);
		});
	},
	businessContact: (req, res) => {
		RegisterBusiness.businessContact(req, res, result => {
			res.send(result);
		})
	},
	businessLocation: (req, res) => {
		RegisterBusiness.businessLocation(req, res, result => {
			res.send(result);
		})
	},
	businessImages: (req, res) => {
		RegisterBusiness.businessImages(req, res, result => {
			console.log('Uploaded!');
			res.send(result);
		})
	}
}