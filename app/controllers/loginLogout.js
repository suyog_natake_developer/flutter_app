const LoginLogout = require('./../models/loginLogout')
module.exports = {
	loginWithOTP: (req, res) => {
		LoginLogout.loginWithOTP(req, res, (result) => {
			res.send(result);
		})
	},
	userOtpVerification: (req, res) => {
		LoginLogout.userOtpVerification(req, res, (result) => {
			res.send(result);
		})
	},
	logoutUser: (req, res) => {
		LoginLogout.logoutUser(req, res, (result) => {
			res.send(result)
		})
	}


}