const database = require('../../database');
const connection = database.db.get;
const Config = require('./../../config');
const jwt = require('jsonwebtoken');
require('dotenv').Config;

const http = require('http')
var urlencode = require('urlencode');
const randomToken = require('random-token')

function generateOtpToken() {
	randomToken.create('abcdefghijklmnopqrstuvwxzyABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
	return randomToken(16);
};

function generateAccessToken(user) {
	return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET)
};


function generateOTP() {
	return Math.floor(Math.random() * 10000);
}

module.exports = {
	loginWithOTP: (req, res, result) => {
		var mobile_number = req.body.mobile_number;

		var queryStatement = `SELECT UM.id,UM.name,UM.mobile_number 
		FROM maratha_pariwaar_schema."Users" 
		AS UM WHERE UM.mobile_number=$1 AND status=1`;
		connection.query(queryStatement, [mobile_number], (err, userInfo) => {
			if (err) {
				console.log(err)
				res.status(500)
				res.json({
					errCode: Config.errCodeError,
					errMessage: "Internal Server Error....",
					data: ""
				})
			}
			if (userInfo.rows.length == 0) {
				console.log(userInfo)
				res.status(403)
				res.json({
					errCode: Config.errCodeError,
					errMessage: "You are not registered... Please Register!!",
					data: ""
				})

			} else {
				if (userInfo.rows[0].mobile_number != 0) {

					const otpToken = generateOtpToken();
					const otp = generateOTP();
					// var expiryDate = new Date().toISOString().split('T')[0];
					var expiryDate = new Date();
					var queryStatement1 = `INSERT INTO maratha_pariwaar_schema."User_OTP" (user_id,otp,token,expiry) VALUES ($1,$2,$3,$4) RETURNING id`;

					connection.query(queryStatement1, [userInfo.rows[0].id, otp, otpToken, expiryDate], (err, result) => {
						if (err) {
							console.log(err)
							res.status(500)
							res.json({
								errCode: Config.errCodeError,
								errMessage: "Internal Server error",
								data: ""
							})
						}

						if (result) {
							// sending an OTP through txtlocal is remainin

							res.status(200)
							res.json({
								errCode: Config.errCodeSuccess,
								errMessage: "OTP send to registred mobile number successfully",
								data: "",
								otpToken: otpToken
							})
							// module.exports.sendMessage(res, userInfo.rows[0].mobile_number, result.rows[0].id, otp);
						}

					})
				}
			}
		})
	},

	userOtpVerification: (req, res, result) => {
		var otp = req.body.otp;
		var otpToken = req.body.otpToken;
		var queryStatement = `SELECT UO.id,UO.user_id,UO.otp,UO.token
		FROM maratha_pariwaar_schema."User_OTP" 
		AS UO WHERE UO.otp = $1 AND UO.token = $2`;
		connection.query(queryStatement, [otp, otpToken], (err, userResult) => {
			if (err) {
				console.log(err);
				res.status(500)
				res.json({
					errCode: Config.errCodeError,
					errMessage: "Internal Server Error",
					data: ""
				})
			}


			if (userResult.length == 0) {
				console.log(userResult.length)
				res.status(403)
				res.json({
					errCode: Config.errCodeError,
					errMessage: "Please Insert Correct OTP",
					data: ""
				})
				return;
			}

			const user = {
				userId: userResult.rows[0].user_id,
			}
			const apiToken = generateAccessToken(user);
			var expiryDate = new Date();
			var queryStatement2 = `INSERT INTO maratha_pariwaar_schema."User_Tokens" (user_id,token,expiry) VALUES ($1,$2,$3) RETURNING id`;

			connection.query(queryStatement2, [userResult.rows[0].user_id, apiToken, expiryDate], (err, tokenResult) => {
				if (err) {
					console.log(err)
					res.status(500)
					res.json({
						errCode: Config.errCodeError,
						errMessage: "Internal Server error",
						data: ""
					})
				}

				if (tokenResult) {
					var userOtpId = userResult.rows[0].id
					var queryStatment3 = `DELETE FROM maratha_pariwaar_schema."User_OTP"
						WHERE id=$1`;
					connection.query(queryStatment3, [userOtpId], (err, finalResult) => {
						if (err) {
							console.log(err);
							res.status(400)
							res.json({
								errCode: Config.errCodeError,
								errMessage: "Internal Server Error...",
								data: ""
							})
						}
						if (finalResult) {
							res.status(200)
							res.json({
								errCode: Config.errCodeSuccess,
								errMessage: "Login successful!!",
								data: "",
								token: apiToken
							})
						}

					})
				}
			})

		})
	},

	logoutUser: (req, res, result) => {
		const authHeader = req.headers['authorization'];
		const token = authHeader && authHeader.split(' ')[1];

		var userId = req._decode.userId;

		// console.log(token, userId);
		var queryStatment = `SELECT UT.id,UT.user_id,UT.token 
		          FROM maratha_pariwaar_schema."User_Tokens"
		          AS UT
		         WHERE UT.user_id = $1 AND UT.token = $2`;
		connection.query(queryStatment, [userId, token], (err, userTokenResult) => {
			if (err) {
				console.log(err);
				res.status(500)
				res.json({
					errCode: Config.errCodeError,
					errMessage: "Internal Server Error",
					data: ""
				})
			}
			if (userTokenResult.rows.length != 0) {

				var queryStatment2 = `DELETE FROM maratha_pariwaar_schema."User_Tokens"
					WHERE user_id=$1 AND token=$2`;

				connection.query(queryStatment2, [userId, token], (err, result) => {
					if (err) {
						console.log(err);
						res.status(400)
						res.json({
							errCode: Config.errCodeError,
							errMessage: "Internal Server Error...",
							data: ""
						})
					}
					if (result) {
						res.status(200)
						res.json({
							errCode: Config.errCodeSuccess,
							errMessage: "Logout successful!!",
							data: "",
						})
					}

				})


			} else {
				res.status(403)
				res.json({
					errCode: Config.errCodeError,
					errMessage: "Please Insert Correct credentials",
					data: ""
				})
			}
		})

	},









	// if (result) {
	// 	module.exports.sendMessage(res, userInfo.rows[0].mobile_number, result.rows[0].id, otp);
	// }



	// sendMessage: (res, phoneNo, userId, otp) => {
	// 	var axios = require("axios").default;
	// 	// var url = "https://api.textlocal.in/send/?apikey=MzQ5NmU0NWEyYmYyYTA1YTg1NjE4NTY1M2ZmYmJjMGQ=&sender=Jijauu&numbers=91" + phoneNo + "&message=\"मराठा परिवार\"%0a" + otp + " हा आपला लॉग इन OTP आहे.%0a\"जय जिजाऊ जय शिवराय\"&unicode=1"
	// 	var url = "https://api.textlocal.in/send/?apikey=MzQ5NmU0NWEyYmYyYTA1YTg1NjE4NTY1M2ZmYmJjMGQ=&sender=Jijauu&numbers=91" + phoneNo + "&message= मराठा परिवार " + otp + " हा आपला लॉग इन OTP आहे.जय जिजाऊ जय शिवराय &unicode=1"


	// 	const tlClient = axios.create({
	// 		baseURL: "https://api.textlocal.in/",
	// 		params: {
	// 			apiKey: "MzQ5NmU0NWEyYmYyYTA1YTg1NjE4NTY1M2ZmYmJjMGQ", //Text local api key
	// 			sender: "Jijauu"
	// 		}
	// 	});




	// 	var options = {
	// 		method: 'POST',
	// 		url: url,
	// 		headers: {
	// 			'x-rapidapi-host': 'api.textlocal.in',
	// 			'x-rapidapi-key': 'MzQ5NmU0NWEyYmYyYTA1YTg1NjE4NTY1M2ZmYmJjMGQ'
	// 		}
	// 	};

	// 	axios.request(options).then(function (response) {
	// 		if (response) {
	// 			var sms = tlClient.post("/send", otp, phoneNo);
	// 			console.log(sms)
	// 			res.status(200)
	// 			res.json({
	// 				errCode: Config.errCodeSuccess,
	// 				errMessage: "OTP send to your registered mobile number",
	// 				data: ""
	// 			})
	// 		}
	// 	}).catch(function (error) {
	// 		console.log(error)
	// 		console.log("Problems in sending message");

	// 	});


	// },






	///maratha parivar sms  api


	// sendMessage: (res, phoneNo, userId, otp) => {
	// 	var userId = userId;
	// 	var message = "मराठा परिवार" + otp + "हा आपला लॉग इन OTP आहे. जय जिजाऊ जय शिवराय ";
	// 	var msg = urlencode(message);
	// 	var toNumber = phoneNo;
	// 	var username = "Ajit Kerure";
	// 	var hash = 'MzQ5NmU0NWEyYmYyYTA1YTg1NjE4NTY1M2ZmYmJjMGQ=';

	// 	// The hash key could be found under Help->All Documentation->Your hash key. Alternatively you can use your Textlocal password in plain text.
	// 	var sender = 'txtlcl';
	// 	var data = 'username=' + username + '&hash=' + hash + '&sender=' + sender + '&numbers=' + toNumber + '&message=' + msg;
	// 	// var data = 'username=' + username + '&hash=' + hash + '&sender=' + sender + '&numbers=' + toNumber + '&message=' + message;
	// 	console.log("data", data);

	// 	var options = {
	// 		host: 'api.textlocal.in', path: '/send?' + data
	// 	};
	// 	// api.textlocal.in/send/?apikey=MzQ5NmU0NWEyYmYyYTA1YTg1NjE4NTY1M2ZmYmJjMGQ=&sender=Jijauu&numbers=91"+${toNumber}+"&message=\"मराठा परिवार\"%0a"+${otp}+" हा आपला लॉग इन OTP आहे.%0a\"जय जिजाऊ जय शिवराय\"&unicode=1
	// 	callback = function (response) {
	// 		var body = '';
	// 		response.on('data', function (chunk) {
	// 			body += chunk;
	// 		});
	// 		response.on('end', function () {
	// 			// res.end(JSON.stringify({ success: 'success' }));
	// 			var textLocalResponse = JSON.parse(body);
	// 			if (textLocalResponse.status == 'failure') {
	// 				result({ status: false, message: "Failed to send message to user.." });
	// 			} else {
	// 				result({ status: true, message: "Message send to user successfully.." });
	// 			}
	// 		});
	// 	}
	// 	http.request(options, callback).end();
	// 	// https://api.textlocal.in/send/?apikey=MzQ5NmU0NWEyYmYyYTA1YTg1NjE4NTY1M2ZmYmJjMGQ=&sender=Jijauu&numbers=91"+PhoneNo+"&message=\"मराठा परिवार\"%0a"+Otp+" हा आपला लॉग इन OTP आहे.%0a\"जय जिजाऊ जय शिवराय\"&unicode=1";
	// },
}



// check the if the  userid and token is present or not
// if the it is present move forward , if it is not then throw a message that error message
// success delete the jwt token and id  
// success --> otp generate --> on insert success --> phone OTP send --> API (verify the OTP)
// On the successfule verify --> delete that OTP --> success message "Login is successful" 

