const database = require('../../database');
const connection = database.db.get;
const Config = require('../../config');

module.exports = {
	userRegistration: (req, res, result) => {

		var name = req.body.name;
		var mobile_number = req.body.mobile_number;
		var gender = req.body.gender;


		if (req.body.email != "") {
			var email = req.body.email;
		} else {
			var email = null;
		}

		if (req.body.dob != "") {
			var dob = req.body.dob;
		} else {
			var dob = null;
		}

		if (req.body.blood_group != "") {
			var blood_group = req.body.blood_group;
		} else {
			var blood_group = null;
		}

		if (req.body.city != "") {
			var city = req.body.city;
		} else {
			var city = null;
		}

		if (req.body.state != "") {
			var state = req.body.state;
		} else {
			var state = null;
		}

		if (req.body.country != "") {
			var country = req.body.country;
		} else {
			var country = null;
		}

		if (req.body.pincode != "") {
			var pincode = req.body.pincode;
		} else {
			var pincode = null;
		}

		if (req.body.education != "") {
			var education = req.body.education;
		} else {
			var education = null;
		}

		if (req.body.profession != "") {
			var profession = req.body.profession;
		} else {
			var profession = null;
		}

		// 1 for Married, 2 for Unmarried, 3 for divorce, 4 widowed

		if (req.body.marital_status != "") {
			var marital_status = req.body.marital_status;
		} else {
			var marital_status = null;
		}

		var status = "1";

		var queryStatement1 = `SELECT UM.name, UM.mobile_number FROM maratha_pariwaar_schema."Users" AS UM WHERE UM.mobile_number = $1 AND status=1`

		connection.query(queryStatement1, [mobile_number], (err, result) => {
			if (err) {
				console.log(err);
				res.status(500)
				res.json({
					errCode: Config.errCodeError,
					errMessage: "Internal Server Error",
					data: ""
				});
			}

			if (result.rows.length != 0) {
				res.status(403)
				res.json({
					errCode: Config.errCodeError,
					errMessage: "This mobile number is already present please try with another",
					data: ""
				});

			} else {
				var queryStatement = `INSERT INTO maratha_pariwaar_schema."Users" (name,mobile_number, email, dob, blood_group, city, state, country, pincode, education, profession, marital_status, status, gender) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14) RETURNING id`;

				connection.query(queryStatement, [name, mobile_number, email, dob, blood_group, city, state, country, pincode, education, profession, marital_status, status, gender], (err, result) => {
					if (err) {
						console.log(err);
						res.status(500)
						res.json({
							errCode: Config.errCodeError,
							errMessage: "Internal Server Error",
							data: ""
						});

					}

					if (result) {
						res.status(200)
						res.json({
							errCode: Config.errCodeSuccess,
							errMessage: "User added successfully",
							data: "",
							userId: result.rows[0].id.toString()
						})

					}
				});
			}
		});
	}
}