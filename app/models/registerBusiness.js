const database = require('../../database');
const connection = database.db.get;
const Config = require('../../config');
const jwt = require('jsonwebtoken');
require('dotenv').Config;

module.exports = {
	registerBusiness: (req, res, result) => {
		var title = req.body.title;
		var address = req.body.address;
		var city = req.body.city;
		var country = req.body.country;

		if (req.body.state != "") {
			var state = req.body.state;
		} else {
			var state = null;
		}

		if (req.body.pincode != "") {
			var pincode = req.body.pincode;
		} else {
			var pincode = null;
		}

		if (req.body.landmark != "") {
			var landmark = req.body.landmark;
		} else {
			var landmark = null;
		}


		if (req.body.description != "") {
			var description = req.body.description;
		} else {
			var description = null;
		}


		if (req.body.keywords != "") {
			var keywords = req.body.keywords;
		} else {
			var keywords = null;
		}


		var added_by_user_id = req._decode.userId;

		var added_on = new Date();


		var queryStatement1 = `INSERT INTO maratha_pariwaar_schema."Businesses" (title,address, city, state, country, pincode,  description, keywords,added_by_user_id,landmark, added_on) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11) RETURNING id`
		connection.query(queryStatement1, [title, address, city, state, country, pincode, description, keywords, added_by_user_id, landmark, added_on], (err, result) => {

			if (err) {
				console.log(err);
				res.status(500)
				res.json({
					errCode: Config.errCodeError,
					errMessage: "Internal Server Error...!!",
					data: ""
				})
			}
			if (result) {
				res.status(403)
				res.json({
					errCode: Config.errCodeSuccess,
					errMessage: "Business Register Successfully",
					data: ""
				})
			}
		})

	},

	businessContact: (req, res, result) => {
		var id = req.body.id;
		var business_id = req.body.business_id;
		var mobile_number = req.body.mobile_number;

		if (req.body.contact_number != "") {
			var contact_number = req.body.contact_number;
		} else {
			var contact_number = null;
		}

		if (req.body.email != "") {
			var email = req.body.email;
		} else {
			var email = null;
		}


		if (req.body.website != "") {
			var website = req.body.website;
		} else {
			var website = null;
		}

		if (req.body.whatsapp_number != "") {
			var whatsapp_number = req.body.whatsapp_number;
		} else {
			var whatsapp_number = null;
		}

		var queryStatement = `SELECT B.id
		FROM maratha_pariwaar_schema."Businesses"
		AS B
	   WHERE B.id = $1`

		connection.query(queryStatement, [id], (err, businessInfo) => {

			if (err) {
				res.status(500)
				res.json({
					errCode: Config.errCodeError,
					errMessage: "Internal Server Error",
					data: ""
				})
			}
			if (businessInfo.rows.length != 0) {
				var queryStatement2 = `SELECT BCD.business_id
			FROM maratha_pariwaar_schema."Businesses_Contact_details"
			AS BCD
		   WHERE BCD.business_id = $1`;

				connection.query(queryStatement2, [business_id], (err, contactInfo) => {
					if (err) {
						console.log(err)
					}
					if (businessInfo.rows[0].id == business_id) {
						console.log(contactInfo)
						var queryStatement3 = 'INSERT INTO maratha_pariwaar_schema."Businesses_Contact_details" (business_id, mobile_number, contact_number, email, website, whatsapp_number) VALUES ($1,$2,$3,$4,$5,$6) RETURNING id';

						connection.query(queryStatement3, [business_id, mobile_number, contact_number, email, website, whatsapp_number], (err, result) => {

							if (err) {
								res.status(500)
								res.json({
									errCode: Config.errCodeError,
									errMessage: "Internal Server Error...!!",
									data: ""
								})

							}

							if (result.rows.length != 0) {
								console.log(result.rows)

								res.status(200)
								res.json({
									errCode: Config.errCodeSuccess,
									errMessage: "Contact  Added Successfully",
									data: result.rows[0].id
									// data: businessInfo.rows[0].id;
								})
							}
						})


					} else {
						console.log(result)
						res.status(500)
						res.json({
							errCode: Config.errCodeError,
							errMessage: "Please Insert Correct Business Id",
							data: ""
						})

					}



				})
			}


		})
	},
	businessLocation: (req, res, result) => {
		var id = req.body.id;
		var business_id = req.body.business_id;
		var lattitude = req.body.lattitude;
		var longitude = req.body.longitude;

		var queryStatement3 = `SELECT B.id
			FROM maratha_pariwaar_schema."Businesses"
			AS B
		   WHERE B.id = $1`;

		connection.query(queryStatement3, [id], (err, businessInfo) => {

			if (err) {
				console.log(err);
				res.status(500)
				res.json({
					errCode: Config.errCodeError,
					errMessage: "Internal Server Error...!!",
					data: ""
				})
			}

			if (businessInfo.rows.length != 0) {
				var queryStatement4 = `SELECT B.business_id
			FROM maratha_pariwaar_schema."Businesses_Locations"
			AS B
		   WHERE B.business_id = $1`;

				connection.query(queryStatement4, [business_id], (err, result) => {

					if (err) {
						console.log(err);

					}
					if (businessInfo.rows[0].id == business_id) {
						var queryStatement5 = `INSERT INTO maratha_pariwaar_schema."Businesses_Locations" (business_id,lattitude,longitude) VALUES ($1,$2,$3) RETURNING id`;
						connection.query(queryStatement5, [business_id, lattitude, longitude], (err, result) => {
							if (err) {
								res.status(500)
								res.json({
									errCode: Config.errCodeError,
									errMessage: "Internal Server Error!!",
									data: ""
								})
							}
							if (result.rows.length != 0) {
								res.status(200)
								res.json({
									errCode: Config.errCodeSuccess,
									errMessage: "Location  Added Successfully",
									// data: result.rows[0].id
									data: businessInfo.rows[0].id
								})
							}
						})
					}
					else {
						console.log(result)
						res.status(500)
						res.json({
							errCode: Config.errCodeError,
							errMessage: "Please Insert Correct Business Id",
							data: ""
						})

					}
				})


			}
		})

	},
	businessImages: (req, res, result) => {
		var business_id = req.body.business_id;
		console.log(business_id)

	}


}



