const Joi = require('@hapi/joi');
const Config = require('./../../../config')


module.exports = {
	registerBusiness: (req, res, next) => {
		const schema = Joi.object().keys({
			title: Joi.string().min(1).required(),
			address: Joi.string().min(1).required(),
			city: Joi.string().min(1).required(),
			state: Joi.string().allow("", null),
			country: Joi.string().min(1).required(),
			pincode: Joi.string().allow("", null),
			landmark: Joi.string().allow("", null),
			description: Joi.string().allow("", null),
			keywords: Joi.string().allow("", null),

		});

		if (schema.validate(req.body).error) {
			let error = schema.validate(req.body).error
			res.status(400)
			res.json({
				errCode: Config.errCodeError,
				errMessage: "Invalid request parameters..",
				data: error
			});

		} else {
			next();
		}
	},
	businessContact: (req, res, next) => {
		const schema = Joi.object().keys({
			id: Joi.string().min(1).required(),
			business_id: Joi.string().min(1).required(),
			mobile_number: Joi.string().min(1).required(),
			contact_number: Joi.string().allow("", null),
			email: Joi.string().allow("", null),
			website: Joi.string().allow("", null),
			whatsapp_number: Joi.string().allow("", null),
		});
		if (schema.validate(req.body).error) {
			let error = schema.validate(req.body).error
			res.status(400)
			res.json({
				errCode: Config.errCodeError,
				errMessage: "Invalid request parameters..",
				data: error
			});

		} else {
			next();
		}
	},
	businessLocation: (req, res, next) => {
		const schema = Joi.object().keys({
			id: Joi.string().min(1).required(),
			business_id: Joi.string().min(1).required(),
			lattitude: Joi.string().min(1).required(),
			longitude: Joi.string().min(1).required(),

		});
		if (schema.validate(req.body).error) {
			let error = schema.validate(req.body).error
			res.status(400)
			res.json({
				errCode: Config.errCodeError,
				errMessage: "Invalid request parameters..",
				data: error
			});

		} else {
			next();
		}
	},
	businessImages: (req, res, next) => {
		const schema = Joi.object().keys({
			business_id: Joi.string().min(1).required(),
		});

		if (schema.validate(req.body).error) {
			let error = schema.validate(req.body).error
			res.status(400)
			res.json({
				errCode: Config.errCodeError,
				errMessage: "Invalid request parameters..",
				data: error
			});

		} else {
			next();
		}
	},
}
