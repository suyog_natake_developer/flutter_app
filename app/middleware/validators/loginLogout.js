const Joi = require('@hapi/joi');
const Config = require('./../../../config')
module.exports = {
	loginWithOTP: (req, res, next) => {
		const schema = Joi.object().keys({
			mobile_number: Joi.string().min(1).required(),
		});

		if (schema.validate(req.body).error) {
			let error = schema.validate(req.body).error
			res.status(400)
			res.json({
				errCode: Config.errCodeError,
				errMessage: "Invalid request parameters..",
				data: error
			});

		} else {
			next();
		}
	},

	userOtpVerification: (req, res, next) => {
		const schema = Joi.object().keys({
			otp: Joi.string().min(1).required(),
			otpToken: Joi.string().min(1).required(),
		});
		if (schema.validate(req.body).error) {
			res.send({
				status: false,
				message: "Invalid request parameters.."
			});
		} else {
			next();
		}
	},

	logoutUser: (req, res, next) => {
		const schema = Joi.object().keys({
			userId: Joi.string().min(1).required(),
			userToken: Joi.string().min(1).required(),
		});
		if (schema.validate(req.body).error) {
			res.send({
				status: false,
				message: "Invalid request parameters.."
			});
		} else {
			next();
		}
	}
}
