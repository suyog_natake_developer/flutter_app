const Joi = require("@hapi/joi");
const Config = require('../../../config');


module.exports = {
	userRegistration: (req, res, next) => {
		const schema = Joi.object().keys({
			name: Joi.string().min(1).required(),
			mobile_number: Joi.string().min(1).required(),
			email: Joi.string().allow("", null),
			gender: Joi.string().allow("", null),
			dob: Joi.string().allow("", null),
			blood_group: Joi.string().allow("", null),
			city: Joi.string().allow("", null),
			state: Joi.string().allow("", null),
			country: Joi.string().allow("", null),
			pincode: Joi.string().allow("", null),
			education: Joi.string().allow("", null),
			profession: Joi.string().allow("", null),
			marital_status: Joi.string().allow("", null),

		});
		if (schema.validate(req.body).error) {
			let error = schema.validate(req.body).error
			// errorResponse(res, "Invalid request parameteres", 400, Config.errCodeError, error);
			res.status(400);
			res.json({
				errCode: Config.errCodeError,
				errMessage: "Invalid request parameteres",
				data: error
			})

		} else {
			next();
		}

	}
}