const Config = require('./../../config')
const database = require('./../../database')
const connection = database.db.get;
const jwt = require('jsonwebtoken');
const jwt_decode = require('jwt-decode');
const Joi = require('@hapi/joi')


const authenticateUserToken = async (req, res, next) => {

	const authHeader = req.headers['authorization'];
	const token = authHeader && authHeader.split(' ')[1];

	if (!token) {
		res.status(401)
		res.json({
			errCode: Config.errCodeError,
			errMessage: "Failed to authorize user!!",
			data: ""
		});
		return;
	}

	try {
		req._decode = jwt_decode(token);
		const userInformation = {
			userId: req._decode.userId,
			username: req._decode.username
		}

		var queryStatment = `SELECT UO.id,UO.user_id,UO.token 
		          FROM maratha_pariwaar_schema."User_Tokens"
		          AS UO
		         WHERE UO.token = $1`;
		connection.query(queryStatment, [token], (err, result) => {
			if (err) {
				console.log(err)
				res.status(400);
				res.json({
					errCode: Config.errCodeError,
					errMessage: "Something went wrong, please try again",
					data: ""
				})
			}

			if (result.length == 0) {
				console.log(result)
				res.status(401);
				res.json({
					errCode: Config.errCodeError,
					errMessage: "Failed to authorize user",
					data: ""
				})
			} else {

				jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
					if (err) {
						console.log(err)
						res.status(401)
						res.json({
							errCode: Config.errCodeError,
							errMessage: "Failed to authorize user...",
							data: ""
						});
					} else {
						// console.log("I am came here")
						next();
					}
				})
			}
		})
	} catch (error) {
		console.log(error);
		res.status(500)
		res.json({
			errCode: errCodeError,
			errMessage: "something went wrong!! Please try again later",
			data: ""
		})
	}


}


module.exports = { authenticateUserToken }









// 	try {
		// 		req._decode = jwt_decode(token);
		// 		console.log(req._decode.userId);
		// 		console.log(req._decode.username)
		// 		// const userInformation = await postgres.app_tokens.findMany({
		// 		// 	where: {
		// 		// 		user_id: req._decode.userId,
		// 		// 		app_token: token,
		// 		// 		status: 1
		// 		// 	},
		// 		// 	select: {
		// 		// 		id: true,
		// 		// 	},
		// 		// })

		// 		// if (userInformation.length == 0) {
		// 		// 	res.status(401)
		// 		// 	res.json({
		// 		// 		errCode: errCodeError,
		// 		// 		errMessage: "Failed to authorize user.......",
		// 		// 		data: ""
		// 		// 	})
		// 		// } else {

		// 		// 	jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
		// 		// 		if (err) {
		// 		// 			res.status(401)
		// 		// 			res.json({
		// 		// 				errCode: errCodeError,
		// 		// 				errMessage: "Failed to authorize user...",
		// 		// 				data: ""
		// 		// 			});
		// 		// 		} else {
		// 		// 			next();
		// 		// 		}
		// 		// 	})
		// 		// }
		// 	} catch (error) {
		// 		console.log(error);
		// 		res.status(500)
		// 		res.json({
		// 			errCode: errCodeError,
		// 			errMessage: "something went wrong!! Please try again later",
		// 			data: ""
		// 		})
		// 	}


///tested  jwt



		// req._decode = jwt_decode(token);

		// const userInformation = {
		// 	userId: req._decode.userId,
		// 	username: req._decode.username
		// }

		// console.log(userInformation)

		// jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
			// 	if (err) {
			// 		res.status(401)
			// 		res.json({
			// 			errCode: Config.errCodeError,
			// 			errMessage: "Failed to authorize user...",
			// 			data: ""
			// 		});
			// 	} else {
			// 		console.log("I am Come into this")
			// 		next();
			// 	}
			// })