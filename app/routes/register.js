const RegisterUser = require('../controllers/register')
const validationMiddleware = require('../middleware/validators/register')

module.exports = function (app) {
	app.post('/api/user/register', validationMiddleware.userRegistration, (req, res) => {

		RegisterUser.userRegistration(req, res);

	})

}