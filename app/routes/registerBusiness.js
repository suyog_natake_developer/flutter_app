const RegisterBusiness = require('./../controllers/registerBusiness')
const validationMiddleware = require('./../middleware/validators/registerBusiness')
const { authenticateUserToken } = require('./../middleware/authentication')
const upload = require('./../../testMulter')
module.exports = function (app) {
	app.post('/api/user/register-business', authenticateUserToken, validationMiddleware.registerBusiness, (req, res) => {
		RegisterBusiness.registerBusiness(req, res);
	});

	app.post('/api/user/business/contact-details', authenticateUserToken, validationMiddleware.businessContact, (req, res) => {
		RegisterBusiness.businessContact(req, res);
	});

	app.post('/api/user/business/location', authenticateUserToken, validationMiddleware.businessLocation, (req, res) => {
		RegisterBusiness.businessLocation(req, res);
	})

	app.post('/api/user/business/images', authenticateUserToken, validationMiddleware.businessImages, upload.array('file', 12), (req, res) => {
		RegisterBusiness.businessImages(req, res);
	})

}
