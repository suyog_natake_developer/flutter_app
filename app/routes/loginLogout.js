const LoginLogout = require('./../controllers/loginLogout')
const { authenticateUserToken } = require('./../middleware/authentication')
const validationMiddleware = require('../middleware/validators/loginLogout')



module.exports = function (app) {
	app.post('/api/candidate/login', validationMiddleware.loginWithOTP, (req, res) => {
		LoginLogout.loginWithOTP(req, res);
	});

	app.post('/api/candidate/verify-otp', validationMiddleware.userOtpVerification, (req, res) => {
		LoginLogout.userOtpVerification(req, res);
	});

	app.put('/api/candidate/logout', authenticateUserToken, validationMiddleware.logoutUser, (req, res) => {
		LoginLogout.logoutUser(req, res);
	})
}