const Register = require('./register');
const LoginLogout = require('./loginLogout');
const User = require('./user')
const RegisterBusiness = require('./registerBusiness');

const database = require('./../../database');
const connection = database.db.get;
const cors = require('cors');
require("dotenv").config();
const { port, NODE_ENV, ORIGIN } = require('./../../config')
const { API_ENDPOINT_NOT_FOUND_ERR, SERVER_ERR } = require('../../errors')

module.exports = function (app) {
	app.get("/", function (req, res, next) {
		res.send("Directory access is forbidden..");
	});

	Register(app);
	User(app);
	LoginLogout(app);
	RegisterBusiness(app);
}