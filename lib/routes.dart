import 'package:flutter/widgets.dart';
import 'package:flutter_app/ui/sign_in/sign_in_screen.dart';
import 'package:flutter_app/ui/business/business_main_screen.dart';

// We use name route
// All our routes will be available here
final Map<String, WidgetBuilder> routes = {
  SignInScreen.routeName: (context) => SignInScreen(),
  business_main_screen.routeName:(context) =>business_main_screen(),
};
