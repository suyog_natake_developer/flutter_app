import 'package:flutter/material.dart';
import 'package:flutter_app/animations/fade_animations.dart';
const kPrimaryColor = Color(0XFF6A62B7);

void main() {
  runApp(MaterialApp(
      debugShowCheckedModeBanner: false, home: register_business_screen()));
}

class register_business_screen extends StatefulWidget {
  @override
  _register_business_screenState createState() => _register_business_screenState();
}

class _register_business_screenState extends State<register_business_screen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(color: Color.fromRGBO(132, 169, 172, 1.0)),
        child: Column(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(top: 100),
              child: const FadeAnimation(
                2,
                Text(
                  "Registration Page",
                  style: TextStyle(
                    fontSize: 35,
                    fontFamily: 'Raleway',
                    color: Colors.white,
                    letterSpacing: 3,
                  ),
                ),
              ),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(15),
                        topRight: Radius.circular(15))),
                margin: const EdgeInsets.only(top: 50),
                child: SingleChildScrollView(
                    child: Column(
                      children: [
                        const SizedBox(
                          height: 60,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 32),
                          child: Material(
                            elevation: 2.0,
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            child: TextField(
                              onChanged: (String value) {},
                              cursorColor: Color.fromRGBO(32, 64, 81, 1.0),
                              decoration: InputDecoration(
                                hintText: "Business Name",
                                prefixIcon: Material(
                                  elevation: 0,
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(30)),
                            /*      child: Icon(
                                    Icons.person,
                                    color: Color.fromRGBO(32, 64, 81, 1.0),
                                  ),*/
                                ),
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 25, vertical: 13),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 32),
                          child: Material(
                            elevation: 2.0,
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            child: TextField(
                              onChanged: (String value) {},
                              cursorColor: Color.fromRGBO(32, 64, 81, 1.0),
                              decoration: InputDecoration(
                                hintText: "Business Email",
                                prefixIcon: Material(
                                  elevation: 0,
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(30)),
                                        child: Icon(
                                    Icons.email,
                                    color: Color.fromRGBO(32, 64, 81, 1.0),
                                  ),
                                ),
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 25, vertical: 13),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 32),
                          child: Material(
                            elevation: 2.0,
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            child: TextField(
                              onChanged: (String value) {},
                              cursorColor: Color.fromRGBO(32, 64, 81, 1.0),
                              decoration: InputDecoration(
                                hintText: "Business Contact",
                                prefixIcon: Material(
                                  elevation: 0,
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(30)),
                                  child: Icon(
                                    Icons.phone,
                                    color: Color.fromRGBO(32, 64, 81, 1.0),
                                  ),
                                ),
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 25, vertical: 13),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 32),
                          child: Material(
                            elevation: 2.0,
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            child: TextField(
                              onChanged: (String value) {},
                              cursorColor: Color.fromRGBO(32, 64, 81, 1.0),
                              decoration: InputDecoration(
                                hintText: "Business GST No",
                                prefixIcon: Material(
                                  elevation: 0,
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(30)),
                                  child: Icon(
                                    Icons.format_list_numbered,
                                    color: Color.fromRGBO(32, 64, 81, 1.0),
                                  ),
                                ),
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 25, vertical: 13),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 32),
                          child: Material(
                            elevation: 2.0,
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            child: TextField(
                              onChanged: (String value) {},
                              cursorColor: Color.fromRGBO(32, 64, 81, 1.0),
                              decoration: InputDecoration(
                                hintText: "Business FSSAI No",
                                prefixIcon: Material(
                                  elevation: 0,
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(30)),
                                  child: Icon(
                                    Icons.format_list_numbered,
                                    color: Color.fromRGBO(32, 64, 81, 1.0),
                                  ),
                                ),
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 25, vertical: 13),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 32),
                          child: Material(
                            elevation: 2.0,
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            child: TextField(
                              onChanged: (String value) {},
                              cursorColor: Color.fromRGBO(32, 64, 81, 1.0),
                              decoration: InputDecoration(
                                hintText: "Business Description",
                                prefixIcon: Material(
                                  elevation: 0,
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(30)),
                                  child: Icon(
                                    Icons.format_list_numbered,
                                    color: Color.fromRGBO(32, 64, 81, 1.0),
                                  ),
                                ),
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 25, vertical: 13),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Padding(
                            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.all(Radius.circular(100)),
                                color: Color.fromRGBO(32, 64, 81, 1.0),
                              ),
                              child: FlatButton(
                                child: Text(
                                  "Register",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: 'Raleway',
                                      fontSize: 18),
                                ),
                                onPressed: () {},
                              ),
                            )),
                      ],
                    )),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
