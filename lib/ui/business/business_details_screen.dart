import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_app/resources//images.dart';

void main() {
  runApp(business_details_screen());
}

class business_details_screen extends StatefulWidget {
  @override
_business_details_screen createState() => _business_details_screen();
}

class _business_details_screen extends State<business_details_screen> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Details Screen',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
        fontFamily: 'Raleway',
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomePage(),
    );
  }
}
class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back),
        ),
        title: Text(
          "Busuniess Details",
          style: TextStyle(color: Colors.white),
        ),
      ),

      body: ListView(
        children: <Widget>[
          CarouselSlider(
            items: [
              //1st Image of Slider
              Container(
                margin: EdgeInsets.all(6.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8.0),
                  image: DecorationImage(
                    image: new AssetImage(maratha),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              //2nd Image of Slider
              Container(
                margin: EdgeInsets.all(6.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8.0),
                  image: DecorationImage(
                    image: new AssetImage(maratha),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              //3rd Image of Slider
              Container(
                margin: EdgeInsets.all(6.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8.0),
                  image: DecorationImage(
                    image: new AssetImage(maratha),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              //4th Image of Slider
              Container(
                margin: EdgeInsets.all(6.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8.0),
                  image: DecorationImage(
                    image: new AssetImage(maratha),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              //5th Image of Slider
              Container(
                margin: EdgeInsets.all(6.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8.0),
                  image: DecorationImage(
                    image: new AssetImage(maratha),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ],
            //Slider Container properties
            options: CarouselOptions(
              height: 250.0,
              enlargeCenterPage: true,
              autoPlay: true,
              aspectRatio: 16 / 9,
              autoPlayCurve: Curves.fastOutSlowIn,
              enableInfiniteScroll: true,
              autoPlayAnimationDuration: Duration(milliseconds: 800),
              viewportFraction: 0.8,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: FittedBox(
              child: Material(
                color: Colors.white,
                borderRadius: BorderRadius.circular(14.0),
                shadowColor: Colors.white,
                elevation: 2.0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 180.0,
                              height: 180.0,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(14.0),
                                child: Image(
                                  image: AssetImage(aavid),
                                  fit: BoxFit.cover,
                                  // alignment: Alignment.bottomLeft,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Column(children: [
                      Container(
                        margin: const EdgeInsets.only(top: 0.0, right: 50.0),
                        child: Text(
                          'Aavid Software',
                          style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Raleway',
                          ),
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Container(
                            margin: const EdgeInsets.only(
                                top: 5.0, left: 10.0, right: 20.0),
                            child: Text(
                              'Nal Stop, Kothrud Pune',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 18.0,
                                  fontFamily: 'Raleway'),
                              maxLines: 3,
                            ),
                          ),
                        ],
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 7.0, right: 50.0),
                        child: Text(
                          '2 km',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 18.0,
                              fontFamily: 'Raleway'),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 9.0, right: 50.0),
                        child: Text(
                          'Software Development',
                          style: TextStyle(
                            fontSize: 16.0,
                            fontFamily: 'Raleway',
                          ),
                        ),
                      ),
                    ]),
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: FittedBox(
              child: Material(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5.0),
                shadowColor: Colors.white,
                elevation: 2.0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Column(children: [
                              Container(
                                margin: const EdgeInsets.only(
                                    top: 0.0, right: 95.0),
                                child: Text(
                                  'Description',
                                  style: TextStyle(
                                    fontSize: 9.0,
                                    fontFamily: 'Raleway',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              Container(
                                width: 150,
                                margin: EdgeInsets.fromLTRB(5.0, 0.0, 0.0, 5.0),
                                child: Text(
                                  "computer repair,cctv installation,fire system,security system installation and services,",
                                  /*overflow: TextOverflow.ellipsis,*/
                                  maxLines: 3,
                                  style: TextStyle(
                                    fontSize: 8.0,
                                    fontFamily: 'Raleway',
                                  ),
                                ),
                              ),
                            ]),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(5.0, 0.0, 10.0, 5.0),
            child: Text(
              "Products & services",
              /*overflow: TextOverflow.ellipsis,*/
              style: TextStyle(
                fontSize: 15.0,
                fontFamily: 'Raleway',
              ),
            ),
          ),
          new Container(
              height: 250,
              child: ListView.builder(
                itemCount:5,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, i) => Container(
                  width: 200,
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Align(
                            alignment: Alignment.topRight,
                            child: Positioned.fill(
                                top: 5,
                                right: 5,
                                child: GestureDetector(
                                    onTap: () {
                                      /* setState(() {});*/
                                    },
                                    child: Image.asset(
                                      "assets/images/default_blank.png",
                                      height: 30,
                                      width: 30,
                                    ))),
                          ),
                          Container(
                            width: 60,
                            height: 60,
                            // child :  Image.file(File(imageData))
                            decoration: new BoxDecoration(
                                shape: BoxShape.circle,
                                image: new DecorationImage(
                                  fit: BoxFit.fill,
                                  image:new ExactAssetImage("assets/images/default_blank.png"),
                                )),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            "cctv cabling and installation",
                            style: TextStyle(
                                fontSize: 14,
                                fontWeight:
                                FontWeight.bold),
                          ),
                          SizedBox(
                            height: 6,
                          ),
                          Text(
                            "Price:Rs",
                            textAlign: TextAlign.center,
                            maxLines: 2,
                            style: TextStyle(
                                fontFamily:'Raleway',
                                fontSize: 14,
                                color: Colors.grey),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              )),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: FittedBox(
              child: Material(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5.0),
                shadowColor: Colors.white,
                elevation: 2.0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Column(children: [
                              Container(
                                margin: const EdgeInsets.only(
                                    top: 0.0, right: 95.0),
                                child: Text(
                                  'Description',
                                  style: TextStyle(
                                    fontSize: 9.0,
                                    fontFamily: 'Raleway',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              Container(
                                width: 150,
                                margin: EdgeInsets.fromLTRB(5.0, 0.0, 0.0, 5.0),
                                child: Text(
                                  "computer repair,cctv installation,fire system,security system installation and services,",
                                  /*overflow: TextOverflow.ellipsis,*/
                                  maxLines: 3,
                                  style: TextStyle(
                                    fontSize: 8.0,
                                    fontFamily: 'Raleway',
                                  ),
                                ),
                              ),
                            ]),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

