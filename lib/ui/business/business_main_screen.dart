import 'package:flutter/material.dart';
import 'package:flutter_app/resources/images.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_app/ui/business/register_business_screen.dart';
void main() {
  runApp(business_main_screen());
}

class business_main_screen extends StatefulWidget {
  static String routeName = "/business_main_screen";

  @override
  _business_main_screen createState() => _business_main_screen();
}

class _business_main_screen extends State<business_main_screen> {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Details Screen',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        backgroundColor: Colors.white,
        fontFamily: 'Raleway',
        /*    visualDensity: VisualDensity.adaptivePlatformDensity,*/
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Builder(builder: (BuildContext context) {
          return IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {},
          );
        }),
        title: Text("Business Details"),
        actions: [
          IconButton(
              icon: Icon(Icons.person),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => register_business_screen()));
              }),
          IconButton(
              icon: Icon(Icons.filter_list),
              onPressed: () {
                bottomsheets(context);
              }),
        ],
      ),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: FittedBox(
              child: Material(
                color: Colors.white,
                borderRadius: BorderRadius.circular(14.0),
                shadowColor: Colors.white,
                elevation: 2.0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              margin:
                                  EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 20.0),
                              width: 180.0,
                              height: 180.0,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(14.0),
                                child: Image(
                                  image: AssetImage(aavid),
                                  fit: BoxFit.cover,
                                  // alignment: Alignment.bottomLeft,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Column(children: [
                      Container(
                        margin: const EdgeInsets.only(top: 0.0, right: 80.0),
                        child: Text(
                          'Aavid Software',
                          style: TextStyle(
                            fontSize: 25.0,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Raleway',
                          ),
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Container(
                            margin:
                                const EdgeInsets.only(top: 10.0, right: 50.0),
                            child: Text(
                              'Nal Stop, Kothrud Pune',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20.0,
                                  fontFamily: 'Raleway'),
                              maxLines: 3,
                            ),
                          ),
                        ],
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 7.0, right: 220.0),
                        child: Text(
                          '2 km',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 20.0,
                              fontFamily: 'Raleway'),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 10.0, right: 50.0),
                        child: Text(
                          'Software Development',
                          style: TextStyle(
                            fontSize: 20.0,
                            fontFamily: 'Raleway',
                          ),
                        ),
                      ),
                      Column(
                        //* crossAxisAlignment: CrossAxisAlignment.center,*//*
                        children: <Widget>[
                          //*  SizedBox(width: 50, height: 25),*//*
                          Container(
                            margin: EdgeInsets.fromLTRB(0.0, 10.0, 80.0, 0.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[
                                Icon(
                                  FontAwesomeIcons.whatsapp, //Icon Size
                                  color: Colors.green, //Color Of Icon
                                  size: 30,
                                ),
                                SizedBox(width: 25),
                                Icon(
                                  FontAwesomeIcons.phoneSquareAlt, //Icon Size
                                  color: Colors.green, //Color Of Icon
                                  size: 30,
                                ),
                                SizedBox(width: 25),
                                Icon(
                                  FontAwesomeIcons.instagram, //Icon Size
                                  color: Colors.red, //Color Of Icon
                                  size: 30,
                                ),
                                SizedBox(width: 25),
                                Icon(
                                  FontAwesomeIcons.facebook, //Icon Size
                                  color: Colors.blue, //Color Of Icon
                                  size: 30,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ]),
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: FittedBox(
              child: Material(
                color: Colors.white,
                borderRadius: BorderRadius.circular(14.0),
                shadowColor: Colors.white,
                elevation: 2.0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              margin:
                                  EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 20.0),
                              width: 180.0,
                              height: 180.0,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(14.0),
                                child: Image(
                                  image: AssetImage(image2),
                                  fit: BoxFit.cover,
                                  // alignment: Alignment.bottomLeft,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Column(children: <Widget>[
                      Container(
                        margin: const EdgeInsets.only(top: 0.0, right: 80.0),
                        child: Text(
                          'Aavid Software',
                          style: TextStyle(
                            fontSize: 25.0,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Raleway',
                          ),
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Container(
                            margin:
                                const EdgeInsets.only(top: 10.0, right: 50.0),
                            child: Text(
                              'Nal Stop, Kothrud Pune',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20.0,
                                  fontFamily: 'Raleway'),
                              maxLines: 3,
                            ),
                          ),
                        ],
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 7.0, right: 220.0),
                        child: Text(
                          '2 km',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 20.0,
                              fontFamily: 'Raleway'),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 10.0, right: 50.0),
                        child: Text(
                          'Software Development',
                          style: TextStyle(
                            fontSize: 20.0,
                            fontFamily: 'Raleway',
                          ),
                        ),
                      ),
                    ]),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void bottomsheets(context) {
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        builder: (context) => Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25),
                    topRight: Radius.circular(25),
                  )),
            ));
  }
}
