import 'package:flutter/material.dart';
import 'package:flutter_app/size_config.dart';

import 'components/body.dart';


void main() {
  runApp(otp_screen());
}

class otp_screen extends StatelessWidget {
/*  static String routeName = "/otp";*/
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Body(),
    );
  }
}
