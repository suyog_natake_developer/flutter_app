import 'package:flutter/material.dart';
import 'package:flutter_app/ui/login_screen.dart';
import '../animations/fade_animations.dart';

void main() {
  runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      home: forgot_pass_screen()));
}

class forgot_pass_screen extends StatefulWidget {
  static String routeName = "/forgot_pass_screen";
  @override
  _forgot_pass_screenState createState() => _forgot_pass_screenState();
}

class _forgot_pass_screenState extends State<forgot_pass_screen> {

  bool _isObscure=true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(color: Color.fromRGBO(132, 169, 172, 1.0)),
        child: Column(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(top: 100),
              child: const FadeAnimation(
                2,
                Text(
                  "Forgot Password",
                  style: TextStyle(
                    fontSize: 35,
                    fontFamily: 'Raleway',
                    color: Colors.white,
                    letterSpacing: 3,
                  ),
                ),
              ),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(15),
                        topRight: Radius.circular(15))),
                margin: const EdgeInsets.only(top: 50),
                child: SingleChildScrollView(
                    child: Column(
                      children: [
                        const SizedBox(
                          height: 60,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 32),
                          child: Material(
                            elevation: 2.0,
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            child: TextField(
                              onChanged: (String value) {},
                              cursorColor: Color.fromRGBO(32, 64, 81, 1.0),
                              decoration: InputDecoration(
                                hintText: "Name",
                                prefixIcon: Material(
                                  elevation: 0,
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(30)),
                                  child: Icon(
                                    Icons.person,
                                    color: Color.fromRGBO(32, 64, 81, 1.0),
                                  ),
                                ),
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 25, vertical: 13),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 32),
                          child: Material(
                            elevation: 2.0,
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            child: TextField(
                              onChanged: (String value) {},
                              cursorColor: Color.fromRGBO(32, 64, 81, 1.0),
                              decoration: InputDecoration(
                                hintText: "Email",
                                prefixIcon: Material(
                                  elevation: 0,
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(30)),
                                  child: Icon(
                                    Icons.email,
                                    color: Color.fromRGBO(32, 64, 81, 1.0),
                                  ),
                                ),
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 25, vertical: 13),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 32),
                          child: Material(
                            elevation: 2.0,
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            child: TextField(
                              onChanged: (String value) {},
                              cursorColor: Color.fromRGBO(32, 64, 81, 1.0),
                              decoration: InputDecoration(
                                hintText: "Password",
                                prefixIcon: Material(
                                  elevation: 0,
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(30)),
                                  child: Icon(
                                    Icons.lock,
                                    color: Color.fromRGBO(32, 64, 81, 1.0),
                                  ),
                                ),
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 25, vertical: 13),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 32),
                          child: Material(
                            elevation: 2.0,
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            child: TextField(
                              obscureText: _isObscure,
                              cursorColor:Color.fromRGBO(32, 64, 81, 1.0),
                              decoration: InputDecoration(
                                prefixIcon: Material(
                                  elevation: 0,
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(30)),
                                  child: Icon(
                                    Icons.lock_clock_sharp,
                                    color: Color.fromRGBO(32, 64, 81, 1.0),
                                  ),
                                ),
                                border: InputBorder.none,
                                hintText: 'confirm password',
                                suffixIcon: IconButton(
                                    icon: Icon(
                                        _isObscure ? Icons.visibility : Icons
                                            .visibility_off
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        _isObscure = !_isObscure;
                                      });
                                    }
                                ),
                                /*  prefixIcon: Icon(Icons.lock),*/


                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 25,
                        ),
                        Padding(
                            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.all(Radius.circular(100)),
                                color: Color.fromRGBO(32, 64, 81, 1.0),
                              ),
                              child: FlatButton(
                                child: Text(
                                  "Forgot",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: 'Raleway',
                                      fontSize: 18),
                                ),
                                onPressed: () {},
                              ),
                            )),
                        SizedBox(height:15.0),
                        GestureDetector(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context)=>login_screen()));
                          },
                          child: Text.rich(
                            TextSpan(
                                text: 'Login here?',
                                children: [
                                  TextSpan(
                                    text: 'login',
                                    style: TextStyle(
                                        color:Colors.blue,
                                        fontFamily: 'Raleway'
                                    ),
                                  ),
                                ]
                            ),
                          ),
                        ),
                      ],
                    )),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
