import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_app/resources/Strings.dart';
import 'package:flutter_app/resources/images.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

void main() {
  runApp(farming_main_screen());
}

class farming_main_screen extends StatefulWidget {
  @override
  _farming_main_screen createState() => _farming_main_screen();
}

class _farming_main_screen extends State<farming_main_screen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(farming),
      ),
      body: ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: FittedBox(
              child: Material(
                color: Colors.white,
                borderRadius: BorderRadius.circular(14.0),
                shadowColor: Colors.white,
                elevation: 14.0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 300.0,
                              height: 300.0,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(30.0),
                                child: Image(
                                  image: AssetImage(all_img),
                                  width: 50,
                                  height: 200,
                                  fit: BoxFit.cover,
                                  // alignment: Alignment.bottomLeft,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Column(children: <Widget>[
                      Row(
                        children: <Widget>[
                          Container(
                            margin: const EdgeInsets.only(
                                top: 5.0, left: 10.0, right: 30.0),
                            child: Text(
                              'शेतीचे सारे आवजरे भाड्याने मिळतील.',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 18.0,
                                  fontFamily: 'Raleway'),
                              maxLines: 3,
                            ),
                          ),
                        ],
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 7.0, right: 200.0),
                        child: Text(
                          '₹ 2000',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 18.0,
                              fontFamily: 'Raleway'),
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Container(
                            margin:EdgeInsets.fromLTRB(0.0,10.0, 200.0,0.0),
                            child: IconButton(
                                onPressed: () {},
                                icon: Icon(
                                  FontAwesomeIcons.whatsapp,
                                  color: Colors.green,
                                ),
                            ),

                          ),
                        ],
                      ),
                    ]),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

buildSocialButton({IconData, icon}) {}
