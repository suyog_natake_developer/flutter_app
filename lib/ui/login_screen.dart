
import 'package:flutter/material.dart';
import '../animations/fade_animations.dart';
import 'forgot_pass_screen.dart';
import 'registration_screen.dart';

void main() {
  runApp(MaterialApp(debugShowCheckedModeBanner: false,
      home: login_screen()));
}

class login_screen extends StatefulWidget {
  @override
  _login_screenState createState() => _login_screenState();
}

class _login_screenState extends State<login_screen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(color: Color.fromRGBO(132, 169, 172, 1.0)),
        child: Column(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(top: 100),
              child: const FadeAnimation(
                2,
                Text(
                  "Login Page",
                  style: TextStyle(
                    fontSize: 35,
                    fontFamily: 'Raleway',
                    color: Colors.white,
                    letterSpacing: 3,
                  ),
                ),
              ),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(15),
                        topRight: Radius.circular(15))),
                margin: const EdgeInsets.only(top: 100),
                child: SingleChildScrollView(
                    child: Column(
                  children: [
                    const SizedBox(
                      height: 100,
                    ),
                    StreamBuilder<String>(
                        /*          stream: login.email,*/
                        builder: (context, snapshot) {
                      return Padding(
                        padding: EdgeInsets.symmetric(horizontal: 32),
                        child: Material(
                          elevation: 2.0,
                          borderRadius: BorderRadius.all(Radius.circular(30)),
                          child: TextField(
                            /*      onChanged:login.emailChanged,*/
                            cursorColor: Color.fromRGBO(32, 64, 81, 1.0),
                            decoration: InputDecoration(
                              hintText: "Email",
                              prefixIcon: Material(
                                elevation: 0,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(30)),
                                child: Icon(
                                  Icons.email,
                                  color: Color.fromRGBO(32, 64, 81, 1.0),
                                ),
                              ),
                              border: InputBorder.none,
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 25, vertical: 13),
                            ),
                          ),
                        ),
                      );
                    }),
                    SizedBox(
                      height: 30,
                    ),
                    StreamBuilder<String>(
                        /* stream: login.password,*/
                        builder: (context, snapshot) {
                      return Padding(
                        padding: EdgeInsets.symmetric(horizontal: 32),
                        child: Material(
                          elevation: 2.0,
                          borderRadius: BorderRadius.all(Radius.circular(30)),
                          child: TextField(
                            /*        onChanged:login.passwordChanged,*/
                            cursorColor: Color.fromRGBO(32, 64, 81, 1.0),
                            decoration: InputDecoration(
                              hintText: "Password",
                              prefixIcon: Material(
                                elevation: 0,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(30)),
                                child: Icon(
                                  Icons.lock,
                                  color: Color.fromRGBO(32, 64, 81, 1.0),
                                ),
                              ),
                              border: InputBorder.none,
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 25, vertical: 13),
                            ),
                          ),
                        ),
                      );
                    }),
                    SizedBox(
                      height: 30,
                    ),
                    Padding(
                        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(100)),
                            color: Color.fromRGBO(32, 64, 81, 1.0),
                          ),
                          child: FlatButton(
                            child: Text(
                              "Login",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'Raleway',
                                  fontWeight: FontWeight.w700,
                                  fontSize: 18),
                            ),
                            onPressed: () {},
                          ),
                        )),
                    SizedBox(height: 5),
                    Center(
                      child: Container(
                        child: FlatButton(
                          child: Text(
                            'Forgot Password',
                            style: TextStyle(
                              color: Color.fromRGBO(32, 64, 81, 1.0),
                              fontFamily: 'Raleway',
                            ),
                          ),
                          textColor: Colors.white,
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        forgot_pass_screen()));
                          },
                        ),
                      ),
                    ),
                    SizedBox(height: 15.0),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => registration_screen()));
                      },
                      child: Text.rich(
                        TextSpan(text: 'Don`t have an Account?', children: [
                          TextSpan(
                            text: 'SignUp',
                            style: TextStyle(
                                color: Colors.blue, fontFamily: 'Raleway'),
                          ),
                        ]),
                      ),
                    ),
                  ],
                )),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
